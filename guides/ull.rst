
.. Global macros

.. include:: /macros.rst

.. Specific macros

.. |MakeMKV| replace:: :program:`MakeMKV`


###############################################
  Playing with VLC 4.0 and low delay scenarii
###############################################

Purpose
=======

This guide explains a few tricks to achieve some glass-to-glass low-delay scenarii with |VLC| 4.0.

Tested on:

* :program:`Microsoft Windows 10`, with :program:`VLC 4.0 nightly 20200123`,
* :program:`Debian 10`, with roughly the same version of |VLC|.

The basic options
=================

By default, |VLC| is a very conservative player.

As a consequence, when it tries to read a network stream, a lot of parameters are set to compensate various issues that can occur on a real-life situation:

* bandwidth fluctuations
* network lags
* bad connectivity/network cuts

But that does not mean it cannot be used in some low-delay playback: you just have to change a few parameters for that.

The first options you will have to play with are:

* :option:`--low-delay` : activates the low-delay behavior. It will try to minimize the delay through the whole playback chain (demuxers, packetization and decoders).
* :option:`--network-caching=\<time is ms\>` : modifies the network input buffer size. A small value (in milliseconds) will reduce the delay a lot, but any network lag/change can be deadly for the playback. The default value is 1000 (so 1 second).
* :option:`--clock-jitter=\<time is ms\>` : modifies the jitter tolerance of the player; an acceptance window configured to automatically grow in VLC each time data is late, until a maximum value. The default value, 5000 (5 seconds) means the player will accept a 5 seconds drift (between the first estimation of its internal clock and the current time) before considering that there is a problem and trying to reset its internal clock (and the stream read).

.. note::

   **There is no good default value for low-delay**

   Having a smooth experience with a low-delay constraint is completely dependant on the network devices properties, the stream protocol used to cast, the media formats and codecs.

   So, unless you are a multimedia expert, you will not be able to guess the correct parameters. You will have to test.


More tweaks
===========

Disabling hardware acceleration
-------------------------------

If your CPU has enough power, you can try to deactivate the hardware acceleration. A lot of hardware decoders are starting to decode after receiving several frames, resulting in an additional delay in the whole playback pipeline.

The additional options you can use are then:

* :option:`--no-hw-dec` : disables the hardware decoding. VLC will choose the software decoder implementation, usually provided by the `libavcodec` library.
* :option:`--avcodec-threads=1` : reduces the pool of threads used by `libavcodec` to one. The default behavior of `libavcodec` is to "buffer" one input frame per thread, which also leads to additional delay.

Microsoft Windows and exclusive mode
------------------------------------

On :program:`Microsoft Windows`, one way to minimize the audio buffering is to force the `exclusive` audio mode.

The `exclusive` audio mode forces the operating system to accept only one program to play audio simultaneously. The related options are:

* :option:`--wasapi-exclusive` : activate the audio exclusive option (:program:`Microsoft Windows` only)
* :option:`--audio-resampler=none` : do not activate the audio resampler. This will send the audio data "untouched" to the sound card driver.
* :option:`--no-audio-time-stretch` : do not correct the audio signal based on time fluctuations on the internal clock.

.. warning::

   **Exclusive mode, audio passthrough, and no sound**

   If you activate the audio exclusive options, you may end up to have no sound at all during playback. One of the consequences of sending the audio data to the sound card without any additional filter is that your source audio signal could not be compatible with the audio formats your DAC or sound card can understand. In this case, you should keep only the :option:`--wasapi-exclusive` option, and let VLC perform some conversions.

MPEG-TS PCR relationship with delay
-----------------------------------

If you try to setup a low-delay scenario with some MPEG-TS streams (for example, sending a MPEG-TS stream on UDP on a multicast IP address, and trying to play it as fast as possible with a VLC as a receiver), you might have some issues depending on the program clock (PCR) of the stream.

As a very inaccurate and partial explanation: MPEG-TS live streams are regularly sending information about a reference clock, called the Program Clock Rerence (PCR), that is expected to:

Synchronization
   synchronize decoders (player) time with source.

Milestone
   be a milestone `T` in time to know that all data for all streams arrived for time `T`.

To satisfy the `synchronization` condition, your PCR needs to be transmitted accurately and regurlarly.

VLC will try to play the stream no matter what, but to be "sure" to play it, it will deal with late synchronization by adding some compensation delay to ensure the playback is stable. You will then see some ``PCR reset`` log events in VLC logs (note that this is not the only scenario where you can find this type of message).

To statisfy the `milestone` condition, the PCR for a time `T` needs to be transmitted after all data for that time `T`, and in most streams, it is safely offset by 100 or 200ms. Otherwise, data would likely be considered "too late" when received. 

It is not rare that the default PCR offset is not compatible with a low-delay scenario.

To workaround those issues, you can bypass the stream PCR information and set your own PCR time reference:

* :option:`--no-ts-trust-pcr` : tells to use audio or video data time instead or PCR, with the risk of losing regularity and not satifying the `milestone` condition (see next option).
* :option:`--ts-generated-pcr-offset=\<time in ms\>` : offsets the non PCR timebase, consequence of :option:`--no-ts-trust-pcr`, to be able to deal with other streams and satisfy the `milestone` condition. For example, if there is a difference of 40ms between audio and video tracks, this option needs to be at least equal or greater than this value.

