
.. Global macros

.. include:: /macros.rst

#####################################
  Using VLC as audio/video feedback
#####################################

Purpose
=======

This guide describes a litlle trick to use a |VLC| and a camera as a delayed feedback.

Tested on: :program:`GNU/Linux distribution`, with :program:`VLC 3.0.17`.

Configuration the additional camera delay
=========================================

 + Open the VLC Media Player.
 + Go to :menuselection:`Tools --> Preferences` (Keyboard shortcut is Ctrl+P).
 + Display the Advanced Settings by clicking on :guilabel:`All` option.
 + Click the :guilabel:`Input / Codecs` item and scroll down to the :guilabel:`Advanced` section.
 + The option you are looking for is :guilabel:`Live capture caching (ms)`, defined in milliseconds. For example, if you want to add a 10 seconds delay, change the default value to 10000.
 + Click on the :guilabel:`Save` button.
 + Restart VLC Media Player.

Now, when selecting a video capture source with |VLC|, you will have plenty of time to look at what happened a few seconds earlier.

.. warning::

   The additional delay will be saved for future use, so do not forget to reset the preferences through :guilabel:`Reset Preferences` in :menuselection:`Tools --> Preferences` when you do not need it anymore!


